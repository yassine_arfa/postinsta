//
//  ViewController.swift
//  postInsta
//
//  Created by Ryo on 06/02/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profilIV: UIImageView!
    @IBOutlet var imageLikes: [UIImageView]!
    
    // Principal Image
    @IBOutlet weak var imagePost: UIImageView!
    
    //Buttons under the Post Image
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var paperplaneButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    // number of like
    @IBOutlet weak var likesLabel: UILabel!
    // number of comment
    @IBOutlet weak var postDescLabel: UILabel!
    
    
    var isLiked = false
    var newProfile: UIImageView?
    var baseLikes = 175
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfilIV(profilIV)
        imageLikes.forEach{
            (image) in setupProfilIV(image)
            
        }
         setupDoubleTap()
        profilIV.isUserInteractionEnabled = true
        updateLikeLabel()
        setupAttributedString("Ryo Miyamoto", "Une journée sur ma plage préférée: Tangerine", ["Var", "Love", "PicOfTheDay"])
    }
    
    func setupAttributedString(_ username: String, _ desc: String, _ hastags: [String]) {
        let attributedString = NSMutableAttributedString(string: username + ": ", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)])
        
        let secondString = NSAttributedString(string: desc, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)])
        
        attributedString.append(secondString)
        
        var hastagString = " "
        hastags.forEach { (hash) in
            let str = "#" + hash + " "
            hastagString.append(str)
        }
        let thirdString = NSAttributedString(string: hastagString, attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.systemBlue])
        attributedString.append(thirdString)
        postDescLabel.attributedText = attributedString
    }

    
    func setupProfilIV(_ iv: UIImageView) {
        iv.layer.cornerRadius = iv.frame.height / 2
        iv.layer.borderColor = UIColor.systemBackground.cgColor
        iv.layer.borderWidth = 2
    }

    func setupDoubleTap() {
        imagePost.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(addOrRemoveLike))
        tap.numberOfTapsRequired = 2
        imagePost.addGestureRecognizer(tap)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let first = touches.first {
            if first.view == profilIV {
                if newProfile == nil {
                    newProfile = UIImageView(frame: view.bounds)
                    newProfile?.image = UIImage(named: "profil")
                    newProfile?.contentMode = .scaleAspectFill
                    view.addSubview(newProfile!)
                }
            }
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        newProfile?.removeFromSuperview()
        newProfile = nil
            }
    
    
    @IBAction func addOrRemoveLike() {
        isLiked.toggle()
        let imageString = isLiked ? "heart.fill" : "heart"
        likeButton.setImage(UIImage(systemName: imageString), for: .normal)
        print("I like !")
        updateLikeLabel()
    }
    
    func updateLikeLabel() {
        baseLikes = isLiked ? 176 : 175
        let likesString = "\(baseLikes) j'aime"
        likesLabel.text = likesString
    }
    
//    @IBAction func buttonAction(_ sender: UIButton) {
//        switch sender {
//        case messageButton: print("sent message")
//        case paperplaneButton: print("sent to friend")
//        case bookmarkButton: print("I save")
//        
//        default:
//            print("Hors option")
//        }
//    }
    
    

    @IBAction func likePressed(_ sender: Any) {
        addOrRemoveLike()
    }
    

    
    
    @IBAction func otherButtonPressed(_ sender: UIButton) {
        switch sender.tag {
        case 1: print("send a message")
        case 2: print("share")
        case 3: print("add to my collection")
            
        default: return
            
        }
    }
    
    
}

